# Computing Tutorials

Tutorials on the command line, vim, Eddie, working with a UNIVA/SunGridEngine scheduler, programming in general, et cetera.


The tutorials exist in two forms: as markdown documents, and as Jupyter notebooks. They can be found in the corresponding directories.

Current topics available include
  *  An introduction to gnu screen;
  *  Bash string manipulation; and
  *  Array jobs on a SunGridEngine/Univa scheduler.
  
Additionally, reference documents are available for: 
  *  Basic UNIX commands; and
  *  An example SunGridEngine/Univa job script.

This list is being actively expanded, and topic recommendations are welcome.