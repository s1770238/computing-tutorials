{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# So you want to use gnu screen???\n",
    "\n",
    "### by Bailey Harrington\n",
    "\n",
    "### Day after St Patrick's Day, 2020 — Day 3 of coronavirus-induced isolation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`gnu screen` is a terminal window manager. It allows you to do an **awful lot**. This tutorial is not going to cover that. It **is** going to teach you how to use `screen` to:\n",
    "\n",
    "- have multiple shell sessions active *without having to have multiple shells open*;\n",
    "- name your shell sessions based on what they are doing;\n",
    "- have a session on a remote server that persists if you lose your connection or logout; and\n",
    "  - this extends to interactive sessions, such as qlogins, which you will now be able to reaccess. WOW.\n",
    "\n",
    "It will also show how you can make using `screen` more closely resemble your normal shell set-up. \n",
    "  \n",
    "So. First things first. Open a console/terminal/shell window. However you use the command line.\n",
    "\n",
    "(There is an implicit `Enter` after all of these commands. They won't all produce characters you see on the display, but remember to hit `Enter` anyway.)\n",
    "\n",
    "### To create a new, named, screen instance type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "screen -R <name>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will take you to a `screen` window. Depending on your system settings, this may or may not look the same as your regular shell. (If it doesn't, and you want it to, see my example `.screenrc` file at the end.)\n",
    "\n",
    "From here, you can use the window as you normally would (mostly; I'll discuss why this is not entirely true shortly).\n",
    "\n",
    "### Introducing the screen 'control key'\n",
    "#### *Pro tip: this is not **exactly** the same as the control key on your keyboard.\n",
    "\n",
    "You may or may not be aware that some 'full page applications' that run inside the shell (`vim`, `less`, `history`, to name a few) have their own set of keyboard commands/shortcuts that help to make these tools *extremely* powerful. Well, so does `screen`.\n",
    "\n",
    "**However,** you can't just type these when the screen is open. You first have to *tell screen you want to use a screen command* (rather than a command line one) by hitting the control sequence.\n",
    "\n",
    "By default with `screen`, this is `Ctrl + a`, that is, you hit the `control` and 'a' keys **simultaneously**. Then release before typing whatever command you want.\n",
    "\n",
    "(Now, you may be aware that `Ctrl + a` is, in many shells, the sequence used to navigate to the beginning of the command line prompt. In my `.screenrc file`, I hve included a line to remap the 'control key' so this behaviour is not lost.\n",
    "\n",
    "For now...\n",
    "\n",
    "### Detaching a(n attached) screen\n",
    "\n",
    "In order to detach the current screen hit `Ctrl + a` (or whatever you've set the control key to), release, and type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ":detach"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should return you to the shell where you invoked the screen, almost like you'd never left!\n",
    "\n",
    "The screen has not been terminated; we've simply detached it (the equivalent of setting it aside to pick back up again later).\n",
    "\n",
    "Now that we have at least one existing screen, there are a few more things we can do.\n",
    "\n",
    "First:\n",
    "\n",
    "### Listing the screens that currently exist\n",
    "\n",
    "To do this, simply type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "screen -ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows a list of all of the screens we have now. What if we want to go back to the one we just made (or to another one)?\n",
    "\n",
    "### Reattaching a screen\n",
    "\n",
    "To do this, type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "screen -d -r <name>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NB: The name here can just be the name you assigned. You don't need to include the number appended to its front end.\n",
    "\n",
    "Now, to actually terminate a screen (end it forever, rather than just detach) we...\n",
    "\n",
    "### Kill screen\n",
    "\n",
    "But really. In order to kill the current screen you can just type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Or you can go for a the control key option and hit `Ctrl + a` (or whatever you've set the control key to), release, and type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(It stands for kill.)\n",
    "\n",
    "That's all there is to it. Now if you try listing screens, you won't see the one you just murdered. Which is how it should be.\n",
    "\n",
    "So those are the super-basic things about using `screen`. Now, if you actually try using it, you may notice sometimes, `screen` *behaves oddly*.\n",
    "\n",
    "### How to fix `screen`'s, frankly bizarre, behaviour\n",
    "\n",
    "#### First, scrolling.\n",
    "You will very quickly notice that `screen` will not let you scroll up within the window to see things you've run previously, or output. And when the output from a command is long, this can be quite maddening.\n",
    "\n",
    "There are a few ways to work around this. I'll go through them from worst to best (based on my very strong opinions).\n",
    "\n",
    "1. Type `Ctrl + a` (or your modified control key), release, then hit `Escape`. Now you can scroll.\n",
    "2. Hold down `Shift` and scroll as normal.\n",
    "3. Have this line in your `.screenrc` file (it will look like gibberish):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "termcapinfo xterm* ti@:te@"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next two things I will describe will be noticeable if you use a full-screen application, like `vim`, or others (I assume). When you exit said application, rather than disappearing (as is normal, and what should be the way it always works) the file you were just editing will remain visible, but with the command-line prompt at the bottom.\n",
    "\n",
    "There is no good reason I know of for this.\n",
    "\n",
    "Luckily it is easy to fix.\n",
    "\n",
    "### Closing full-screen applications when you are done with them\n",
    "\n",
    "There are two ways to do this. (Really they're the same, but one you can do interactively, and the other takes forethought.)\n",
    "\n",
    "#### In an open screen window\n",
    "\n",
    "Type `Ctrl + a` (or whatever your control sequence is), release, and type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ":altscreen on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you use `vim`, this sort of syntax may look familiar to you. (If not, you should use `vim`!)\n",
    "\n",
    "This will cause full-page things like `vim` to disappear when they close.\n",
    "\n",
    "#### Preemptively\n",
    "\n",
    "Place this line in your `.screenrc` file (and note it does not have the colon this time):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "altscreen on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, when you try to scroll, or type, or anything inside a screen (or shell) window, weird characters start to appear; they aren't the ones you're typing, they appear at an alarming rate, and there's an awful lot of `[ [ [` s.\n",
    "\n",
    "This means something didn't quite terminate correctly.\n",
    "\n",
    "#### Stopping the seemingly spontaneous sequence of punctuation\n",
    "\n",
    "There are two ways you can stop this. In `screen`, you can type `Ctrl + a` (say it with me now: *or whatever you're control sequence is*), release, and then type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Outside of screen, (and possibly also inside it?), you can just type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Et voilà!\n",
    "\n",
    "Now, for my amazing(ly short, but super handy):\n",
    "\n",
    "# `.screenrc` file\n",
    "\n",
    "Save the following lines inside a file named .screenrc inside your home directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#make sure full-page applications close on exit\n",
    "altscreen on\n",
    "#change the control key to z\n",
    "escape ^za\n",
    "#run .bash_profile on startup\n",
    "shell -$SHELL\n",
    "#allow scrolling\n",
    "termcapinfo xterm* ti@:te@"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may notice two lines in here that I have not explained thus far.\n",
    "\n",
    "`escape ^za` is the line that changes the control key. It makes the control sequence `Ctrl + z`.\n",
    "\n",
    "`shell -$SHELL` tells screen to run my `.bash_profile` as well as `.screenrc` upon startup. Lines like this may be added for any of a number of language or other applications' start-up files. If you have edited things like your `.bash_profile` to get things 'just so', you will definitely want to add this.\n",
    "\n",
    "And that's it. If you feel so inclined, there are MANY more things you can do with `screen`. Go crazy! (Between the man page length and coronavirus isolation, it's practically guaranteed!)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Acknowledgements\n",
    "\n",
    "This tutorial was made possible, in part, Carmen Amador, who provided my own introduction to `gnu screen` via a post-it note."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
