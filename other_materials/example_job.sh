#!/bin/sh 
#############################################################################################

# OPTIONS 

## Note: Lines beginning with #$ are active options. Lines beginning with # $ or just # are treated as regular comments.
## This is a common source of error.



##*******  Name and location options  ********
#$ -N tutorial        ## let's you specify a job name that is different from the name of the .sh file
# $ -P proj_name      ## specifies the project to which this job is assigned; can affect the priority
# $ -cwd              ## execute the job from the current working directory
# $ -wd /exports/igmm/eddie/haley-lab/bailey  ## you can also set a different working directory explicitly 
# $ -o __path__       ## the location the automatically-generated output file should go to
# $ -e __path__       ## the location the automatically-generated error file should go to

##*******  Resource specification and options  ********

# $ -l h_vmem=32G     ## -l specifies a resource. this one is how much memory is available
#$ -l h_rt=00:02:00  ## -l specifies a resource. this one is how much time is available (in hours)
# $ -pe mpi 32        ## -pe stands for parallel environment. mpi is one of the available environments;
                      ## 32G is the amount of memory requested
# $ -pe sharedmem 8   ## -pe stands for parallel environment. sharedmem is another such environment;
                      ## here, 8G of memory are requested
# $ -R y              ## y or n; tells the scheduler whether it should reserve nodes as they become available;
                      ## useful for really large or parallel jobs

##*******  Array job options (for doing almost exactly the same thing many times  *********

# $ -t 1-200          ## -t is for setting up array jobs; this example line is for 200 tasks
# $ -tc 10            ## -tc can only be used in conjunction with -t;
                      ## this limits the number of tasks in the job that can be scheduled at the same time (to 10);
                      ## this helps keep from flooding Eddie (it's polite)

##*******  Options relating to submission/scheduling  *********

# $ -h                ## the job will be submitted, but will be held until you change this
# $ -hold_jid __job__ ## the job will be submitted and held until the specified job(s) are complete
# $ -r y              ## y or n; tells the scheduler whether to rerun the job if it fails under unusual conditions

##*******  Options for notifications on job progress  *********

# $ -M email@host.com  ## email address to which to send notification emails
# $ -m baes           ## conditions under which to send email: 'b'egin, 'a'bort (or rescheduled), 'e'nd, 's'uspended


. /etc/profile.d/modules.sh   ### Initialise the environment modules
module load igmm/apps/python/3.7.3


## Begin the script

date "+TIME: %H:%M:%S"

for i in {4..6}; do
    x=$i
    if [[ x -gt 5 ]]
    then
      echo "$x is greater than 5"
    else
      echo "$x might be less than 5"
    fi
done

sleep 2m
